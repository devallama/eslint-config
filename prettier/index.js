module.exports = {
    plugins: ['prettier'],
    extends: ['prettier'],
    parser: '@typescript-eslint/parser',
    rules: {
        /* prettier rules */
        'prettier/prettier': 2
    }
};
