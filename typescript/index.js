module.exports = {
  plugins: ['@typescript-eslint'],
  extends: ['plugin:@typescript-eslint/recommended', 'plugin:import/recommended', 'plugin:import/typescript'],
  parser: '@typescript-eslint/parser',
  settings: {
      "import/resolver": {
          "typescript": true,
          "node": true
      },
      "import/parsers": {
          "@typescript-eslint/parser": [".ts", ".tsx"]
      },
  },
  rules: {
      /* typescript plugin rules */
      'no-extra-parens': 0,
      '@typescript-eslint/no-extra-parens': 0,
      'no-duplicate-imports': 0,
      '@typescript-eslint/typedef': 1,
      '@typescript-eslint/type-annotation-spacing': 2,
      '@typescript-eslint/no-unnecessary-type-arguments': 2,
      '@typescript-eslint/no-unnecessary-condition': 2,
      '@typescript-eslint/no-type-alias': 0,
      'no-magic-numbers': 0,
      '@typescript-eslint/no-magic-numbers': [1, { ignore: [-1, 0, 1, 2], ignoreEnums: true }],
      'no-dupe-class-members': 0,
      '@typescript-eslint/no-dupe-class-members': 1,
      'no-array-constructor': 0,
      '@typescript-eslint/no-array-constructor': 2,
      'brace-style': 0,
      '@typescript-eslint/brace-style': 2,
      'no-invalid-this': 0,
      '@typescript-eslint/no-invalid-this': 2
  },
  overrides: [
      {
          files: ['**/*.tsx'],
          rules: {
              '@typescript-eslint/no-magic-numbers': 0
          }
      }
  ]
};
