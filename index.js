module.exports = {
    extends: ['eslint:recommended'],
    rules: {
        /* typescript plugin rules */
        'no-duplicate-imports': 2,
        'no-magic-numbers': [1, { ignore: [-1, 0, 1, 2], ignoreEnums: true }],
        'no-dupe-class-members': 1,
        'no-array-constructor': 2,
        'brace-style': 2,
        'no-invalid-this': 2
    }
};
